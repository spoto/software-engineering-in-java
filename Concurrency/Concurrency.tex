\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\newcommand{\inputv}[1]{\check{{#1}}}
\newcommand{\outputv}[1]{\hat{{#1}}}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Concurrency}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Concurrency in Java}
\author{Fausto Spoto}
\subtitle{Software Engineering in Java}
\institute{Universit\`a di Verona, Italy}
\date{September 2014}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Why Concurrency Matters}
\begin{itemize}
\item on monocore architectures, it allows one to
      keep the processor busy
      \begin{itemize}
      \item hence its use in all operating systems
      \item and its presence in programming languages: C, Java etc
      \end{itemize}
\item on multicore architectures, it also allows one
      to use all computing cores to solve a single problem
      \begin{itemize}
      \item getting important nowadays
      \end{itemize}
\end{itemize}

\begin{center}
\includegraphics[width=4cm]{pictures/multicore.jpg}
\hspace*{1cm}
\includegraphics[width=4cm]{pictures/shared_resource.jpg}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Why Concurrency Matters More and More}

\begin{greenbox}{Mobile devices must be responsive}
\begin{itemize}
\item code in the user interface thread must terminate quickly,
      or otherwise the user experience gets degraded: long running
      tasks must be delegated to non-user interface threads
\item while a task is running, the user might need to
      start another app, make a call, surf the internet etc.
      The running task must continue in the background
\end{itemize}
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Why Concurrency is Difficult}
\begin{itemize}
\item protocols might be wrong
      \begin{itemize}
      \item race conditions
      \item deadlocks
      \end{itemize}
\item protocols might be inefficient
      \begin{itemize}
      \item livelocks
      \item slower than monothreaded!
      \end{itemize}
\item data can be shared
\item data can be modified
\item data can be cached on each single core
      \begin{itemize}
      \item each core might have a different view of the memory
      \end{itemize}
\end{itemize}

\begin{redbox}{}
Programmers are not enough qualified to deal with concurrency
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Concurrency in Java}
\begin{itemize}
\item it is actually \emph{multithreading}
      \begin{itemize}
      \item a thread is a process with private stack and \alert{shared heap}
      \item extends \texttt{java.lang.Thread} and overrides \texttt{run}
      \item relatively slow to start
      \end{itemize}
\item from Java 1.0:
      \begin{itemize}
      \item \texttt{synchronized} blocks and methods
      \item each object has a lock accessible through
            \texttt{wait}/\texttt{notify}/\texttt{notifyAll}
      \item \texttt{volatile} variables are never kept into caches
      \item a formal Java memory model
      \end{itemize}
\item from Java 1.5:
      \begin{itemize}
      \item improved memory model
      \item \texttt{java.lang.concurrent.*} has many strong classes
            for concurrency
      \item concurrent hashmaps, latches, futures, callables
      \item executor pools recycle threads to avoid their startup cost
      \end{itemize}
\item from Java 1.7:
      \begin{itemize}
      \item fork executor pools share tasks across threads for divide and conquer
      \end{itemize}
\item from Java 1.8:
      \begin{itemize}
      \item parallel streams
      \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Concurrency in Scala}
\begin{itemize}
\item multithreading, again
\item the \texttt{java.lang.concurrent.*} library can still be exploited
\item the \texttt{Akka} library allows easier programming with the actors
      model (also available in Java)
\item no silver bullet around
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\texttt{java.lang.Thread} and \texttt{java.lang.Runnable}}

\begin{greenbox}{}
\begin{verbatim}
public class java.lang.Thread implements java.lang.Runnable {
  public java.lang.Thread();
  public java.lang.Thread(java.lang.Runnable);
  public static java.lang.Thread currentThread();
  public static void sleep(long) throws InterruptedException;
  public void start(); // call this!
  public void run(); // redefine this!
  public void join() throws java.lang.InterruptedException;
  ...
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\begin{verbatim}
public interface java.lang.Runnable {
  public abstract void run();
}
\end{verbatim}
\end{redbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of \texttt{synchronized}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized (expression) {
  body
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}
into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
final temp = expression;
get the lock of temp
body
release the lock of temp
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\texttt{temp} is constant hence lock and unlock are paired
\end{redbox}

\vspace*{1ex}

\begin{redbox}{}
Java's locks are reentrant
\end{redbox}


\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of a \texttt{synchronized} Instance Method}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized T foo(pars) { body }
\end{verbatim}
\end{greenbox}

\vspace*{1ex}
into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  synchronized (this) { body }
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}
that is into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of this
  body
  release the lock of this
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\texttt{this} is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of a \texttt{synchronized} Static Method of Class \texttt{C}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized static T foo(pars) { body }
\end{verbatim}
\end{greenbox}

\vspace*{1ex}
into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
static T foo(pars) {
  synchronized (C.class) { body }
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}
that is into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of C.class
  body
  release the lock of C.class
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\texttt{C.class} is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{The Meaning of \texttt{wait}/\texttt{notify}/\texttt{notifyAll}}

\begin{greenbox}{\texttt{expression.wait()}}
Waits until somebody will notify on the value of \texttt{expression}
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{expression.notify()}}
Wakes up a thread waiting on the value of \texttt{expression},
if any. If more threads are waiting, one of them
is non-deterministically chosen and awakened
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{expression.notifyAll()}}
Wakes up all threads waiting on the value of \texttt{expression}, if any.
The awakened threads must recheck the condition they were waiting for
\end{greenbox}

\vspace*{2ex}

\begin{redbox}{}
These calls must occur only when the thread has already synchronized on the value
of \texttt{expression}
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Programmers Do it Wrong}
\begin{itemize}
\item \texttt{RaceCondition.java}: when racers collide
\item \texttt{Two2One.java}: when one is better than two
\item \texttt{Philosophers.java}: when philosophers hang
%\item \texttt{Relativity.java}: everybody has a different view of the world
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Java Memory Model}

  It clarifies two aspects: atomicity and visibility
  \begin{enumerate}
  \item I want an action to execute as an unbreakable unit
  \item I want a field write to be visible to everybody
  \end{enumerate}

\begin{center}
\includegraphics[width=9cm]{pictures/java-memory-model.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: Single Thread Rule}

  \begin{greenbox}{}
    Each action in a single thread happens-before every action in that thread
    that comes later in the program order
  \end{greenbox}

  \begin{center}
    \includegraphics[width=7.5cm]{pictures/single-thread-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: Monitor Lock Rule}

  \begin{greenbox}{}
    An unlock on a monitor lock (existing \texttt{synchronized}) happens-before
    any subsequent acquiring on the same monitor lock
    (entering \texttt{synchronized})
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/monitor-lock-rule.png}
  \end{center}

\end{frame}

\begin{frame}
\frametitle{Happens-before: Monitor Lock Rule}

\begin{center}
\includegraphics[width=8cm]{pictures/JMM.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: Volatile Field Rule}

  \begin{greenbox}{}
    A write to a \texttt{volatile} field happens-before every subsequent read of that same field. \alert{\texttt{volatile} guarantees visibility but not atomicity! \texttt{f++} is not atomic!}
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/volatile-variable-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: Thread Start Rule}

  \begin{greenbox}{}
    A call to \texttt{Thread.start()} on a thread happens-before every action in the started thread.
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/thread-start-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: Thread Join Rule}

  \begin{greenbox}{}
    All actions in a thread $T$ happen-before any action of another thread $T'$
    after $T'$ successfully performs a \texttt{Thread.join()} on $T$.
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/thread-join-rule.png}
  \end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Singleton Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1.2cm]{pictures/skull.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large unsound}!}
\end{center}

{\small
\begin{verbatim}
public class Singleton {
  private static Singleton singleton;

  private Singleton() {
    // very heavy and memory hungry computation here!
  }

  public static Singleton get() {
    if (singleton == null)
      singleton = new Singleton();

    return singleton;
  }
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Singleton Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1.2cm]{pictures/sleep.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large slow}!}
\end{center}

{\small
\begin{verbatim}
public class Singleton {
  private static Singleton singleton;

  private Singleton() {
    // very heavy and memory hungry computation here!
  }

  public static Singleton get() {
    synchronized (Singleton.class) {
      if (singleton == null)
        singleton = new Singleton();
    }

    return singleton;
  }
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Singleton Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1cm]{pictures/OK.png} \hspace*{2ex} \raisebox{2.3ex}{\alert{\Large OK}!}
\end{center}

\vspace*{-2ex}
{\small
\begin{verbatim}
public class Singleton {
  private static Singleton singleton;

  private Singleton() {
    // very heavy and memory hungry computation here!
  }

  public static Singleton get() {
    if (singleton == null)
      synchronized (Singleton.class) {
        if (singleton == null)
          singleton = new Singleton();
      }

    return singleton;
  }
}
\end{verbatim}}

\end{frame}

\begin{frame}\frametitle{Double-Check Pattern: Is It Sound?}

\begin{pinkbox}{The double-check pattern is in general wrong!}
Field \texttt{singleton} might be seen from another thread before the
state of the new \texttt{Singleton} becomes visible to that same thread
(reordering). See
\url{https://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html}
\end{pinkbox}

\vspace*{2ex}
Let $T$ be the type of field \texttt{singleton}.
The double-check pattern is sound in all following cases:
%
\begin{itemize}
\item if $T$ is a primitive type distinct from \texttt{long} and \texttt{double}
\item if $T$ is an immutable reference type
\item if \texttt{singleton} is declared \texttt{volatile}, but only for Java $\ge$ 1.5 
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Concurrent Generalized Singleton Pattern}

\begin{center}
  \includegraphics[width=1.2cm]{pictures/skull.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large unsound}!}
\end{center}

{\scriptsize
\begin{verbatim}
public static SingletonTree mk
    (boolean value, SingletonTree left, SingletonTree right) {

  SingletonTree result = new SingletonTree(value, left, right);

  SingletonTree old = memory.putIfAbsent(result, result);
  // we do not create an equal tree but use the previous instance
  return old != null ? old : result;
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Concurrent Singleton Pattern}

\begin{center}
  \includegraphics[width=1cm]{pictures/sleep.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large slow}!}
\end{center}

{\scriptsize
\begin{verbatim}
public static SingletonTree mk
    (boolean value, SingletonTree left, SingletonTree right) {

  SingletonTree result = new SingletonTree(value, left, right);
  SingletonTree old;

  synchronized (memory) {
    old = memory.putIfAbsent(result, result);
  }

  // we do not create an equal tree but use the previous instance
  return old != null ? old : result;
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Concurrent Generalized Singleton Pattern}

\begin{center}
  \includegraphics[width=1.0cm]{pictures/OK.png} \hspace*{2ex} \raisebox{2.3ex}{\alert{\Large OK}!}
\end{center}

{\scriptsize
\begin{verbatim}
private final static ConcurrentMap<SingletonTree, SingletonTree> memory
    = new ConcurrentHashMap<>();

public static SingletonTree mk
    (boolean value, SingletonTree left, SingletonTree right) {

  SingletonTree result = new SingletonTree(value, left, right);

  // the following operation is atomic and not necessarily synchronized
  SingletonTree old = memory.putIfAbsent(result, result);
  // we do not create an equal tree but use the previous instance
  return old != null ? old : result;
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Observer Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1.2cm]{pictures/skull.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large unsound, possible livelock}!}
\end{center}

{\small
\begin{verbatim}
private final Set<Observer> observers = new HashSet<>();

@Override public void register(Observer observer) {
  observers.add(observer);
}

@Override public void remove(Observer observer) {
  observers.remove(observer);
}

@Override public void notifyObservers() {
  for (Observer observer: observers)
    observer.notify(...);
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Observer Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1.2cm]{pictures/sleep.jpg} \hspace*{2ex} \raisebox{2.8ex}{\alert{\Large slow}!}
\end{center}

{\small
\begin{verbatim}
private final Set<Observer> observers = new HashSet<>();

@Override public synchronized void register(Observer observer) {
  observers.add(observer);
}

@Override public synchronized void remove(Observer observer) {
  observers.remove(observer);
}

@Override public synchronized void notifyObservers() {
  for (Observer observer: observers)
    observer.notify(...);
}
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
\frametitle{Example: the Observer Pattern in a Concurrent Setting}

\begin{center}
  \includegraphics[width=1.1cm]{pictures/OK.png} \hspace*{2ex} \raisebox{2.3ex}{\alert{\Large OK}!}
\end{center}

{\small
\begin{verbatim}
private final Map<Observer> observers = new ConcurrentHashMap<>();
// or a CopyOnWriteArrayList

@Override public void register(Observer observer) {
  observers.put(observer, observer);
}

@Override public void remove(Observer observer) {
  observers.remove(observer);
}

@Override public void notifyObservers() {
  for (Observer observer: observers.keySet())
    observer.notify(...);
}
\end{verbatim}}

\end{frame}

\begin{frame}
\frametitle{The Thread-Safeness Problem}

\begin{itemize}
\item thread-safe libraries are being developed nowadays
      \begin{itemize}
      \item their classes should be thread-safe
      \end{itemize}
\item but what does thread-safeness mean exactly?
      \begin{itemize}
      \item it can be used in a multithreaded way?
      \item any multithreaded execution can be rephrased as a sequential execution?
      \end{itemize}
\item verifying thread-safeness is still impossible today
      \begin{itemize}
      \item immutable classes are thread-safe!
      \item use thread-safe classes from the Java standard library
      \item for more complex cases, there are heuristics
      \end{itemize}
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Classes from the Standard Java Library}

\begin{greenbox}{Atomic types}
{\small
\begin{verbatim}
public class java.util.concurrent.atomic.AtomicLong extends Number {
  public AtomicLong(long);
  public AtomicLong();
  public final long get();
  public final void set(long);
  public final long getAndSet(long);
  public final boolean compareAndSet(long, long);
  public final long getAndIncrement();
  public final long getAndDecrement();
  public final long getAndAdd(long);
  public final long incrementAndGet();
  public final long decrementAndGet();
  public final long addAndGet(long);
  public java.lang.String toString();
  public int intValue();
  public long longValue();
  ....
}
\end{verbatim}}

\end{greenbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{Classes from the Standard Java Library}

\begin{greenbox}{Concurrent collections}
{\small
\begin{verbatim}
public class java.util.concurrent.ConcurrentHashMap<K, V> {
  public ConcurrentHashMap();
  public ConcurrentHashMap(java.util.Map<? extends K, ? extends V>);
  public boolean isEmpty();
  public int size();
  public V put(K, V);
  ....
  public V putIfAbsent(K, V);  // atomic
  public boolean remove(K key, V value);  // atomic
  public boolean replace(K, V);  // atomic
  public boolean replace(K, V, V);  // atomic
}
\end{verbatim}}

\end{greenbox}

Accesses are synchronized but \textbf{not} on the same lock (split locks) !

Compare with \texttt{java.util.Vector} or with
\texttt{java.util.Collections.synchronizedCollection(wrapped)}
\end{frame}

\begin{frame}
\frametitle{A Complex Example of a Concurrent Algorithm}

\begin{itemize}
\item \texttt{SetOfTrees.java}: sound but monothreaded and slow
\item \texttt{SetOfTreesMultithreaded.java}: livelock!
\item \texttt{SetOfTreesMultithreadedConcurrent.java}: multithreaded, fast but unsound
\item \texttt{SetOfTreesMultithreadedDebugged.java}: multithreaded, sound but slow
\item \texttt{SetOfTreesMultithreadedDebuggedEfficient.java}: multithreaded, fast and sound!
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Classes from the Standard Java Library}

\begin{greenbox}{Producers/Consumers}
{\small
\begin{verbatim}
public class java.util.concurrent.LinkedBlockingQueue<E> {
  public LinkedBlockingQueue();
  public LinkedBlockingQueue(java.util.Collection<? extends E>);
  public LinkedBlockingQueue(int);
  public void put(E);  // blocking
  public E take();  // blocking
  ....
  public int size();
  public boolean offer(E);
  public boolean offer(E, long, java.util.concurrent.TimeUnit);
  public E poll();
  public E poll(long, java.util.concurrent.TimeUnit);
  public E peek();
  ....
}
\end{verbatim}}

\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{An Example of Producers/Consumers}
\begin{itemize}
\item \texttt{FileCrawler.java}: the producers
\item \texttt{FileIndexer.java}: the consumers
\item \texttt{Files.java}: their connection
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Classes from the Standard Java Library}

\begin{greenbox}{Thread-Local Variables}
{\small
\begin{verbatim}
public class java.lang.ThreadLocal<T> {
  public java.lang.ThreadLocal();
  protected T initialValue();
  public T get();
  public void set(T);
  public void remove();
  ....
}
\end{verbatim}}

\end{greenbox}

They are a kind of static variables, that hold a different value for each given thread.

They are often used for the implementation of the singleton pattern, where only a single
object of a given class can be instantiated \emph{per thread}.

\end{frame}

\begin{frame}
\frametitle{An Example of \texttt{ThreadLocal}}
\begin{itemize}
\item \texttt{Session.java}: a singleton session, bound to each thread
\item \texttt{ThreadLocality.java}: starts many threads, each has its own session
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The \texttt{@GuardedBy}/\texttt{@Holding} Annotation}

\begin{greenbox}{\texttt{@GuardedBy}}
States that a field or parameter is only accessed by holding a lock
\begin{itemize}
\item introduced by Brian Goetz
\item used for the NASA PathFinder project
\item (to be) checked by the Checker Framework by Ernst?
\item checker and inferred by the Julia tool
\end{itemize}
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{@Holding}}
States that a method is only called by holding a lock
\begin{itemize}
\item (to be) checked by the Checker Framework by Ernst?
\item checker and inferred by the Julia tool
\end{itemize}
\end{greenbox}

\begin{center}
See \texttt{GuardedByTest.java}
\end{center}

\end{frame}

\begin{frame}\frametitle{Alternatives for \texttt{@GuardedBy}/\texttt{@Holding}}

\begin{greenbox}{\texttt{@GuardedBy/@Holding("this")}}
the lock on the receiver of a non-static field or method must be held
\end{greenbox}

\vspace*{1ex}

\begin{greenbox}{\texttt{@GuardedBy("itself")}}
the lock on the same parameter or field must be held
\end{greenbox}

\vspace*{1ex}

\begin{greenbox}{@GuardedBy/@Holding("field-name")}
the lock on the named field of the receiver of a non-static field or method must be held
\end{greenbox}

\vspace*{1ex}

\begin{greenbox}{@GuardedBy/@Holding("Class.field-name")}
the lock on the named static field of the named class must be held
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{Alternatives for \texttt{@GuardedBy}/\texttt{@Holding}}

\begin{greenbox}{@GuardedBy/@Holding("Class.class")}
the lock of the unique class object representing the class named \texttt{Class} must be held
\end{greenbox}

\vspace*{1ex}

\begin{greenbox}{@GuardedBy/@Holding("foo()")}
the lock on the return value of the named instance method called on the receiver of a non-static field or method must be held. Method \texttt{foo} must return a reference type
\end{greenbox}

\vspace*{1ex}

\begin{greenbox}{@GuardedBy/@Holding("Class.foo()")}
the lock on the return value of the named static method of the named class must be held. Method \texttt{foo} must return a reference type
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{The Julia Static Analyzer}
\begin{itemize}
\item project started in 2003
\item static (compile-time) analysis of Java programs
\item there is a module that checks and infers sound \texttt{@GuardedBy} and \texttt{@Holding}
\item completely automatic
\item specialized for properties of the heap memory
\item static analyses are plugins added to the framework
\item from 2010 is a company: \texttt{http://www.juliasoft.com}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Tools for Concurrent Software Development}

\begin{itemize}
\item the standard \texttt{-Xprof} Java profiler is a basic tool for simple
      profiling: identifies block time and deadlocks
\item the YourKit Java profiler provides detailed information on block time
      and monitor usage and identifies deadlocks
\item static checkers such as Julia and FindBugs
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Conclusion: How to Tame the Beast}

\begin{itemize}
\item use immutable objects
      \begin{itemize}
      \item lessons learned from logic and functional programming have been
            underestimated and completely forgotten
      \item programmers still think that modification is better than reallocation
      \item years of bad influence from C and C++ coding need to be forgotten
      \end{itemize}
\item start programming with parallelism in mind
\item map independent, logical tasks into threads
\item use \texttt{java.lang.concurrent.*} classes
      \begin{itemize}
      \item do not reinvent the wheel
      \end{itemize}
\item use static code analysis
      \begin{itemize}
      \item FindBugs, Julia etc.
      \end{itemize}
\item wait for the next programming language for parallelism (if ever)
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{References}
\begin{greenbox}{}
\begin{center}
\url{http://www.juliasoft.com/checkers/GuardedBy}

\vspace*{2ex}

\includegraphics[width=3cm]{pictures/jcip.jpg}
\hspace*{1cm}
\includegraphics[width=3.15cm]{pictures/wgjd.jpg}
\hspace*{1cm}
\includegraphics[width=3cm]{pictures/android_threading.png}
\end{center}
\end{greenbox}

\end{frame}

\end{document}

