\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{DesignPatterns}
  \usefonttheme[onlysmall]{structurebold}
}

\subtitle{Software Engineering in Java}
\title{Introduction}
\institute{Universit\`a di Verona, Italy}
\date{January 2016}

\setbeamercovered{invisible}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}\frametitle{A Slide about Myself}

\begin{greenbox}{Associate professor, University of Verona}
Working in the area of software development and verification
\begin{itemize}
\item formal methods
\item static analysis
\item abstract interpretation
\item automatic tools
\item Java, Android
\end{itemize}
\end{greenbox}

\begin{center}
  My first startup: \url{www.juliasoft.com}
\end{center}

\begin{greenbox}{Credits}
The very first slides have been partially inspired by previous work by Marco Volpe, Luca Vigan\`o and
William Mitchell (thanks!)
\end{greenbox}
\end{frame}

\begin{frame}[fragile]\frametitle{Download Sites}

\begin{greenbox}{Sources of these slides}
git clone git@bitbucket.org:spoto/software-engineering-in-java.git
\end{greenbox}

\vspace*{5ex}
\begin{greenbox}{Source code of the examples}
git clone git@bitbucket.org:spoto/software-engineering-in-java-examples.git
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{What is Software Engineering?}

\begin{greenbox}{From Wikipedia, the free encyclopedia}
Software engineering is the study and application of engineering to the design, development, and maintenance of software
\end{greenbox}

\begin{itemize}
\item design: think before you code; do not reinvent the wheel
\item development: follow coding standards; use supporting tools
\item maintenance: produce clean code and tests; verify your code
\end{itemize}

\end{frame}

\begin{frame}\frametitle{What is Important in Software?}

\begin{itemize}
\item speed (really?)
\item memory footprint
\item readability
\item abstraction
\item genericity
\item maintenability
\item testability
\item modularity
\end{itemize}

\end{frame}

\begin{frame}\frametitle{Misconceptions about a Programmer}

\begin{itemize}
\item a programmer is a clever person
\item he is a low-level expert
\item he writes very obscure code that works!
\item he only uses \texttt{vi}
\item he is socially impaired
\item he is a man, of course
\end{itemize}

\begin{center}
\includegraphics[width=6cm]{pictures/nerd.jpg}
\end{center}

\end{frame}

\begin{frame}\frametitle{What a Programmer should be}

\begin{itemize}
\item a programmer is a scientist
\item a programmer is an engineer
\item he works over abstractions
\item he writes clean code
\item he uses the full toolbelt
\item he is a social animal
\item no gender bias
\end{itemize}

\begin{center}
\includegraphics[width=6cm]{pictures/love_programming.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Software Development Statistics}

\begin{itemize}
\item the typical software project requires 1-2 years and at least
      500,000 lines of code
\item only between 70-80\% of all projects are successfully completed
\item over the entire development cycle, each person produces on
      average less than 10 lines of code per day
\item during development on average 50-60 errors are found per 1,000
      lines of source code. Typically this drops to around 4 after system
      release
\end{itemize}

\end{frame}

\begin{frame}\frametitle{How Projects Really Work {\normalsize (\url{www.projectcartoon.com})}}

\begin{center}
\includegraphics[width=10.5cm]{pictures/project-tree-swing.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Historical Context: 1960s -- 1970s}
\begin{itemize}
\item Batch software (punch-cards!) with highly restricted memory

\item Simple complexity

\item Low-level, machine-oriented programming languages: Cobol, Fortran,
  Algol

\item Development problems spurred interest in semantics and
  verification questions

\item The term \alert{software crisis} was coined in 1965

\item Initiated research in structured data types leading to ALGOL-W
  (-68), C, Pascal, Ada
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Historical Context: 1970s -- 1980s}
  \begin{itemize}
  \item Technology supports increasingly large projects

  \item Engineering \alert{in the large} leads to new kinds of problems

  \item Challenge: programming in teams

    \begin{itemize}
    \item Development must be split $\Rightarrow$ \alert{decomposition}
      in analysis, specification, coding
    \item Unstructured programming methods don't scale: global data,
      non-modular construction, lack of well-define interfaces
    \end{itemize}
  \item Gradual recognition that software development is difficult!

  \item Evolution of concepts like structured
    programming, stepwise refinement, modularization, abstract
    datatypes, software engineering

  \item Ideas embodied in languages like Pascal, Modula-2, ML, C++,
    Java
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Current Situation --- New Complexities}
 \begin{itemize}
 \item Large scale, distributed, heterogeneous systems, e.g.,
   Internet-centric computing, mobile computing
 \item Concurrent and parallel computing
\item Cheap microsystems = massive distribution
  \begin{itemize}
  \item Typical car has 100s of microprocessors
  \item Computerized control systems are increasingly used in security
    critical applications, e.g., controlling trains, planes, and nuclear
    reactors
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Role of Software Engineering}
\begin{itemize}
\item Is there a `silver bullet' ?  %\vspace*{-10pt}
\begin{center} \huge \sc
\alert{No!}
\end{center}

Software engineering is less clear-cut than, say, 
theoretical computer science.

\item But there are \alert{techniques}, \alert{methods}, and \alert{tools},
   that can reduce the complexity of constructing systems

\item There are also techniques for building specific
   kinds of systems with high degrees of reliability.
   Distribution systems, embedded systems, real-time systems, etc. 
   all have specialized development/validation techniques
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Our Point of View: Development Techniques and Tools}

\begin{enumerate}
\item introduction, waterfall, agile, UML
\item Java and fundamentals of OO design
\item testing (JUnit)
\item general design patterns
\item design patterns for videogames
\item MVC
\item Android design patterns and MVC for Android
\item tools for collaboration (\texttt{git})
%\item lambda expressions and related patterns
%\item concurrency in Java
%\item static analysis (FindBugs, Julia)
\end{enumerate}

\end{frame}

\begin{frame}
\frametitle{Software Development --- Historically}
  \begin{itemize}
  \item The \alert{code-and-fix} development process:
    \begin{enumerate}
    \item Write program
    \item Improve it (debug, add functionality, improve efficiency, ...)
    \item GOTO 1
    \end{enumerate}
  \item Works well for 1-man-projects and simple assignments.

    Unstructured development fine for small projects

  \item Larger projects $\Longrightarrow$ software crisis:
    \begin{itemize}
    \item Poor reliability
    \item Disastrous when programmer quits
    \item Inappropriate for multiple man-year projects
    \item Expectations often differ when the developer isn't the
      end-user
    \item Poor maintainability
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Requirements for Development Process}
  \begin{itemize}
  \item A procedure to \alert{guide} and \alert{control} the entire
    development
  \item Should support developing \alert{high-quality} systems
    \begin{description} 
    \item[Adequacy:] System satisfies desired requirements
    \item[Usability:] Appropriate GUIs, documentation, etc.
    \item[Reliability:] Robustness, security, etc.
    \item[Maintainability:] System easy to modify and improve
    \item[Cost:] Acceptable development costs (time, money, etc.)
    \item[Performance:] Resource use is minimized (or not wasted)
    \end{description}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Software Development Activities}
  \begin{itemize}
  \item Development process: \alert{activities} and \alert{results} of
    software production
  \item Four basic activities:
    \begin{description}
    \item[\alert{Specification:}] Definition of functionality and constraints
    \item[\alert{Design and Implementation:}] Production of system
    \item[\alert{Validation:}] Verification, testing, etc.
    \item[\alert{Evolution/Maintenance:}] Changes and improvements
    \end{description}
  \item Subdivision of activities depends on the particular process
    employed.
    Differs depending on the kind of system built and the organizational
    context
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Generic Software Process Models}

  \begin{description}
  \item [Waterfall model:] Activities staged in phases, each with a
    well-defined deliverable
  \item [Evolutionary development:] Activities are interleaved.  System
    quickly develops from rough specification
  \item [Formal transformation:] Stepwise transformation of a formal
    specification into a program
  \item [Component-based software engineering:] Reuse of existing
    components to assemble the system
  \end{description}

  There are many variants of these models, e.g.\ formal development where
  a waterfall-like process is used but the specification is a formal
  specification that is refined through several stages to an
  implementable design
\end{frame}

\begin{frame}
\frametitle{Waterfall Model (Royce 1970)}
  \begin{center}
    \includegraphics[scale=0.45]{pictures/waterfall.jpg} 
  \end{center}
  
  \begin{itemize}
  \item First process model (also called \alert{phase model})
    \begin{itemize}
    \item The development is decomposed in phases
    \item Each phase is completed before the next starts
    \item Each phase produces a product (document or program)
    \end{itemize}
  \item Enthusiastically welcomed by managers!
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Waterfall Process Assumptions}
  \begin{itemize}
  \item Requirements are known from the start, before design
  \item Requirements rarely change
  \item Design can be conducted in a purely abstract way
  \item Everything will fit nicely together when the time comes
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Problems with the Waterfall}
  \begin{itemize}
  \item The process assumptions typically don't apply!  
    For instance, requirements typical imprecise and mature as
    development advances
    \begin{itemize}\normalsize
    \item The \alert{Big Bang Delivery Theory} is risky: proof of concept
      only at the end!
    \end{itemize}
  \item Too much documentation!
  \item Late deployment hides many risks:
    \begin{itemize}
    \item Technological (``well, I \alert{thought} they would work
      together...'')
    \item Conceptual (``well, I \alert{thought} that's what they wanted...'')
    \item Personnel (took so long, half the team left)
    \item User doesn't \alert{see} anything real until the end, and they
      always \alert{hate} it
    \item Testing comes in too late in the process
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Evolutionary Development}
  \begin{picture}(0,0)
    \put(170,-110){\includegraphics[scale=0.32]{pictures/evolutionary_model.jpg}}
  \end{picture}
  \begin{itemize}
  \item \alert{Idea:} build a prototype \\
    and continually improve it
    \begin{itemize}
    \item Build in customer \\
      feedback at each iteration
    \item All process activities \\
      occur simultaneously
    \end{itemize} 
  \end{itemize}

  \vspace*{5ex}
    \begin{description}
    \item[Exploratory Programming:] Query customer requirements.  First
      implement basic (well-understood) requirements and later add more
      complex ones
    \item [Throw-away-Prototyping:] Build prototype to \alert{understand}
      the customer's requirements.  The prototype and experiments with
      the customer help to \alert{define} the requirements
    \end{description}

\end{frame}

\begin{frame}
\frametitle{Extreme Programming (Kent Beck)}

  \begin{greenbox}{}
        An approach to development based on the development and
          delivery of very small increments of functionality
  \end{greenbox}

  \begin{itemize}
  \item Relies on constant code improvement, user involvement in the
    development team and pairwise programming
  \item A human-centered vision of software development
  \item Tests pinpoint and guide the development effort (\emph{test-first})
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Agile Manifesto (2000)}

  \begin{greenbox}{In software we have come to value:}
    \begin{itemize}
    \item early and continuous delivery of valuable software
    \item frequent delivery of working software
    \item working software is the primary measure of progress
    \item welcome and adapt to changing requirements
    \item demand daily communication between business people and development
    \item build projects around teams of motivated people
    \item allow teams to work at a pace that can be sustained forever
    \item allow teams to organize themselves
    \item allow teams to reflect on their successes and failures
    \item strive for simplicity in design and execution
    \item strive for technical excellence in design and execution
    \end{itemize}
  \end{greenbox}

  XP and SCRUM are nowadays seen as implementations of the Agile Manifesto

\end{frame}

\begin{frame}
\frametitle{Component-based Software Engineering}
  \begin{itemize}
  \item Based on systematic reuse where systems are integrated from
    existing components or COTS (commercial off-the-shelf) systems
  \item Main process stages:
    \begin{itemize}
    \item Component analysis
    \item Requirements modification
    \item System design with reuse
    \item Development and integration
    \end{itemize}
  \includegraphics[scale=0.3]{pictures/component-based.png}
  \item This approach is becoming increasingly used as component
    standards have emerged.
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Testing Process}

  \begin{center}
    \includegraphics[scale=0.35]{pictures/testing.jpg}
  \end{center}
  \begin{description}
  \item[Component or unit testing:]
    Individual components are tested independently
  \item[Integration testing:]
    Testing of the software as a whole
  \item[Acceptance or system testing:]
    Testing with customer data to check that the system meets the customer's needs
  \end{description}
\end{frame}

\begin{frame}
\frametitle{The Universal Modeling Language (UML)}

\begin{redbox}{OMG UML Specification}
The Unified Modeling Language (UML) is a language for
specifying, visualizing, constructing, and documenting the artifacts
of software systems, as well as for business modeling and other
non-software systems
\end{redbox}

\vspace*{2ex}

\begin{greenbox}{Why?}
\begin{itemize}
\item it facilitates construction of models
\item it lets one reason about system behavior
\item it lets one present proposed designs to others
\item it documents key elements of design
\item it is extensible, language- and method-independent
\end{itemize}
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Brief History of UML}

\begin{itemize}
\item Object-oriented programming reached the mainstream of
      programming in the late 1980's and early 1990's
\item the rise in popularity of object-oriented programming was
      accompanied by a profusion of object-oriented analysis and design
      methods, each with its own graphical notation
\item in 1994, Booch and Rumbaugh, then both at Rational, started
      working on a unification of their methods. A first draft of their
      Unified Method was released in October 1995
\item in 1996, Jacobson joined Booch and Rumbaugh at Rational;
      the name UML was coined
\item in 1997 the Object Management Group (OMG) accepted UML as
      an open and industry standard visual modeling language for
      object-oriented systems
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{UML Diagram Types}

\begin{description}
\item[Class Diagram]
  Shows relationships between classes and pertinent information
  about classes themselves
\item[Object Diagram]
  Shows a configuration of objects at an instant in time
\item[Interaction Diagram]
  Shows an interaction between a group of collaborating objects
\item[Use-case Diagram]
  Shows actors, use-cases, and the relationships between them
\item[Package Diagram]
  Shows system structure at the library/package level
\item[Deployment Diagram]
  Shows configuration of hardware and software in a distributed system
\item[\ldots]
\end{description}

\end{frame}

\begin{frame}[fragile]
\frametitle{Class Diagrams}

\begin{verbatim}
class Point {
  private double x, y;
  public Point(double x, double y) {
    this.x = x; this.y = y;
  }
  public double getX( ) { return x; }
  public double getY( ) { return y; }
  public double getDistFromOrigin( ) { ... }
}
\end{verbatim}

\begin{center}
\includegraphics[width=4.3cm]{pictures/point_UML.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Class Diagrams, in many flavors}

\begin{center}
Just the name of the class:\\
\mbox{}\\
\includegraphics[width=1.3cm]{pictures/point_UML_name.png}
\end{center}

\begin{center}
Just the name of the class and the public operations:\\
\mbox{}\\
\includegraphics[width=5cm]{pictures/point_UML_public.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Class Diagrams with Relationships}

\begin{verbatim}
public class Rectangle {
  private double width, height;
  private Point ULCorner;
  public Rectangle(Point ULC, double w, double h) { ... }
  public double getArea() { ... }
  public Point getCenter() { ... }
}
\end{verbatim}

\begin{center}
\includegraphics[width=11cm]{pictures/rectangle_UML.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Class Diagrams with Aggregations}

\begin{greenbox}{Diamond arrows}
When a class may reference several instances of another class, the
link between the two classes is shown with a diamond on the end of
the aggregate class and with multiplicity specifications
\end{greenbox}

\begin{center}
\includegraphics[width=11cm]{pictures/aggregation_UML.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Class Diagrams: Inheritance}

\begin{center}
\includegraphics[width=7cm]{pictures/inheritance_UML.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Class Diagrams: Abstract Methods and Classes}

\begin{center}
\includegraphics[width=12cm]{pictures/abstract_UML.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Class Diagrams: Dependency}

\begin{greenbox}{}
If a change in class B may affect class A, then A depends on B
\end{greenbox}

\vspace*{2ex}

\begin{redbox}{}
Creating an instance of a class, or having a variable or parameter of a class type, creates dependency
\end{redbox}

\begin{center}
\includegraphics[width=7cm]{pictures/dependency_UML.png}
\end{center}

\begin{itemize}
\item A uses a B
\item if A has a B then A uses B
\item a change in B might break A
\item unidirectional (single arrowhead) or bidirectional (more arrowheads)
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Object Diagrams}

\begin{greenbox}{}
An object diagram shows a configuration of objects at a point in time
\end{greenbox}

\begin{verbatim}
Point corner = new Point(3, 4);
Rectangle r = new Reactangle(corner, 5, 10);
\end{verbatim}

\begin{center}
\includegraphics[width=9cm]{pictures/objects_UML.png}
\end{center}

\begin{center}
\includegraphics[width=5cm]{pictures/objects_simplified_UML.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Interaction Diagrams: Collaboration}

\begin{greenbox}{}
One type of interaction diagram is a collaboration diagram, which
is essentially an object diagram augmented with method invocations
\end{greenbox}

\begin{verbatim}
Point p = new Point(3,4);
Rectangle r = new Rectangle(p, 5, 10);
double a = r.getArea();
\end{verbatim}

\begin{center}
\includegraphics[width=7.5cm]{pictures/collaboration_UML.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Interaction Diagrams: Collaboration}

\begin{verbatim}
public class Rectangle {
  ...
  public void draw(Canvas c) {
    double x = ULCorner.getX();
    double y = ULCorner.getY();
    c.drawRect(x, y, width, height);
  }
  ...
}
\end{verbatim}

\vspace*{-6ex}
\begin{center}
\includegraphics[width=9cm]{pictures/collaboration_draw_UML.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Interaction Diagrams: Collaboration}

\begin{center}
\includegraphics[width=12cm]{pictures/collaboration_diagram.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Interaction Diagrams: Sequence}

\begin{greenbox}{}
Another type of interaction diagram is a sequence diagram, which
is essentially an object diagram augmented with method invocations
and the lifelines of their activations
\end{greenbox}

\begin{center}
\includegraphics[width=8.7cm]{pictures/sequence_UML.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Interaction Diagrams: Sequence}

\begin{center}
\includegraphics[width=11cm]{pictures/sequence-diagram-overview.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{References}
\begin{greenbox}{}
\begin{center}
\includegraphics[width=4.2cm]{pictures/UML_distilled.jpg}
\end{center}
\end{greenbox}

\begin{center}
Customers usually aren't too interested in a beautiful set of
diagrams. What they want is good software
\end{center}

\end{frame}

\end{document}
