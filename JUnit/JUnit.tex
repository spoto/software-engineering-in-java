\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}
\usepackage{array}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{JUnit}
  \usefonttheme[onlysmall]{structurebold}
}

\subtitle{Software Engineering in Java}
\title{Testing with JUnit}
\institute{Universit\`a di Verona, Italy}
\date{November 2014}

\setbeamercovered{invisible}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}\frametitle{Testing}

\begin{greenbox}{Testing}
\begin{itemize}
\item a \alert{unit test} is a piece of code that executes a specific functionality of
      the code under test. The percentage of code that gets executed is the \alert{test coverage}
\item an \alert{integration or functional test} tests the integration of more components and
      normally translates user stories into a test suite
\item a \alert{performance test} benchmarks software components
\end{itemize}
\end{greenbox}

\begin{center}
  Tests ensure that code (still) works as intended
\end{center}

\end{frame}

\begin{frame}\frametitle{Test-Driven Development (TDD)}

\begin{center}
\includegraphics[width=9cm]{pictures/TDD.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Test-driven refactoring}

\begin{center}
\includegraphics[width=7cm]{pictures/tests_for_refactoring.jpg}
\end{center}

\end{frame}

\begin{frame}\frametitle{Tests for continuous integration}

\begin{center}
\includegraphics[width=12cm]{pictures/CI.jpg}
\end{center}

\end{frame}

\begin{frame}\frametitle{An example of JUnit test}

\begin{center}
\includegraphics[width=10cm]{pictures/example_test_1.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Specifying the fixture}

  \begin{greenbox}{}
    (Part of) the fixture is very often the same for many tests!
  \end{greenbox}

  \begin{center}
    \includegraphics[width=12cm]{pictures/example_test_1_fixture.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Testing exceptions}

  \begin{center}
    \includegraphics[width=11cm]{pictures/example_test_2.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Another class of tests}

  \begin{center}
    \includegraphics[width=9.5cm]{pictures/example_test_3.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Another Class of Tests}

  \begin{center}
    \includegraphics[width=12cm]{pictures/example_test_4.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Another class of tests}

  \begin{center}
    \includegraphics[width=12cm]{pictures/example_test_5.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{JUnit test suite}

\begin{greenbox}{Selecting tests}
Eclipse allows one to run all tests contained in a given source directory,
but specific lists of tests to run can be built into a \emph{test suite}
\end{greenbox}

\begin{center}
\includegraphics[width=12cm]{pictures/test_suite.png}
\end{center}

\begin{redbox}{}
The order of the execution of the tests is unspecified!
\end{redbox}

\end{frame}

\begin{frame}\frametitle{Running a class of test programmatically}

  \begin{center}
    \includegraphics[width=12cm]{pictures/test_programmatically.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{JUnit annotations}

\begin{center}
\begin{tabular}{l|m{5cm}}
  \texttt{@Test} & identifies as a test a public instance void method with no parameters \\\hline
  \texttt{@Test(expected=exc.class)} & fails if the test method does not throw the given exception \\\hline
  \texttt{@Test(timeout=100)} & fails if the test method takes longer than $100$ milliseconds \\\hline
  \texttt{@Before} & identifies a public instance void method with no parameters as a test fixture:
                     it is run before \emph{each} test \\\hline
  \texttt{@After} & identifies a public instance void method with no parameters as a test clean-up:
                    it is run after \emph{each} test
\end{tabular}
\end{center}

\end{frame}

\begin{frame}\frametitle{JUnit annotations}

\begin{center}
\begin{tabular}{l|m{7cm}}
  \texttt{@BeforeClass} & identifies a public static void method with no parameters as a class warm-up:
                     it is run only once before running \emph{all} tests \\\hline
  \texttt{@AfterClass} & identifies a public static void method with no parameters as a class tear-down:
                     it is run only once after \emph{all} tests have finished \\\hline
  \texttt{@Ignore} & ignores the annotated test
\end{tabular}
\end{center}

\end{frame}

\begin{frame}\frametitle{JUnit assertions}

\begin{center}
\begin{tabular}{m{4cm}|m{7cm}}
  \texttt{fail(msg)} & lets the test fail \\\hline
  \texttt{assertTrue([msg,] condition)} & checks that the given condition holds \\\hline
  \texttt{assertFalse([msg,] condition)} & checks that the given condition holds \\\hline
  \texttt{assertEquals([msg,] expected, actual)} & checks that \texttt{expected.equals(actual)} holds \\\hline
  \texttt{assertEquals([msg,] expected, actual, tolerance)} & checks that two floating point numbers
                    are the same up to the tolerated number of decimals
\end{tabular}
\end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{JUnit assertions}

\begin{center}
\begin{tabular}{m{4cm}|m{7cm}}
  \texttt{assertNull([msg,] reference)} & checks that the given reference is \texttt{null} \\\hline
  \texttt{assertNotNull([msg,] reference)} & checks that the given reference is not \texttt{null} \\\hline
  \texttt{assertSame([msg,] expected, actual)} & checks that \texttt{expected == actual} holds \\\hline
  \texttt{assertNotSame([msg,] expected, actual)} & checks that \texttt{expected != actual} holds
\end{tabular}
\end{center}

\vspace*{2ex}
\begin{center}
  \texttt{DataTest.java}, \texttt{StudentTest.java} \\
\end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{The Hamcrest library}

\begin{greenbox}{JUnit assertions wrt a matcher}
{\scriptsize\begin{verbatim}
public static void <T> assertThat(T what, Matcher<? super T> matcher)
public static void <T> assertThat(String msg, T what, Matcher<? super T> matcher)
\end{verbatim}}
\end{greenbox}

\begin{itemize}
\item a matcher embeds a data check or allows a sugared syntax
\item Hamcrest is a library that defines a set of general purpose matchers
\item user-specific matchers can be defined
\end{itemize}

\end{frame}

\begin{frame}\frametitle{Hamcrest matchers}

\begin{center}
\begin{tabular}{l|m{8.5cm}}
  {\small\texttt{equalTo}} & {\small\texttt{assertThat(2 + 2, equalTo(4));}} \\\hline
  {\small\texttt{sameInstance}} & {\small\texttt{assertThat(object, sameInstance(sameObject));}} \\\hline
  {\small\texttt{instanceOf}} & {\small\texttt{assertThat("Hello", instanceOf(String.class));}} \\\hline
  {\small\texttt{notNullValue}} & {\small\texttt{assertThat("Hello", notNullValue());}} \\\hline
  {\small\texttt{nullValue}} & {\small\texttt{assertThat(null, nullValue());}} \\\hline
  {\small\texttt{not}} & {\small\texttt{assertThat("Hello",not(instanceOf(Integer.class)));}} \\\hline
  {\small\texttt{is}} & \hspace*{-1.4ex}\begin{tabular}{l}
    pass-through for syntactical sugar:\\
    {\small\texttt{assertThat(null, is(nullValue()));}}
  \end{tabular}
\end{tabular}
\end{center}

\end{frame}

\begin{frame}\frametitle{Hamcrest collecting matchers}

\begin{center}
\begin{tabular}{l|m{11cm}}
  \texttt{allOf} & \texttt{assertThat("Hello", is(allOf(notNullValue(), instanceOf(String.class), equalTo("Hello"))));} \\\hline
  \texttt{anyOf} & \texttt{assertThat("Hello", is(anyOf(nullValue(), instanceOf(String.class), equalTo("Goodbye"))));}   
\end{tabular}
\end{center}

\end{frame}

\begin{frame}\frametitle{Examples of uses of Hamcrest matchers}

\begin{center}
\includegraphics[width=12cm]{pictures/hamcrest_1.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Examples of uses of Hamcrest matchers}

\begin{center}
\includegraphics[width=12cm]{pictures/hamcrest_2.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{A user-defined matcher}

\begin{center}
\includegraphics[width=12cm]{pictures/newyearsday2015_use.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{A user-defined matcher}

\begin{center}
\includegraphics[width=10.7cm]{pictures/newyearsday2015.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Another user-defined matcher}

\begin{center}
\includegraphics[width=12cm]{pictures/still_use.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Another user-defined matcher}

\begin{center}
\includegraphics[width=9.3cm]{pictures/still.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The Mockito library}

\begin{greenbox}{Mocking for single-class testing}
Mockito allows one to test the behavior of a single class,
with restricted assumptions about its interactions with
other classes. The latter are implemented as
\alert{mocks}, that is, partially implemented objects that
just satisfy the type system
\end{greenbox}

\begin{center}
  Mocking is a particular case of dependency injection
\end{center}

\end{frame}

\begin{frame}\frametitle{Testing an Observable Class: \texttt{WeatherStation}}

For any possible implementation of \texttt{Observer}\ldots

\begin{center}
\includegraphics[width=8cm]{pictures/mockito_declarations.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Testing Observable/Observer Interactions}

\begin{center}
\includegraphics[width=12cm]{pictures/mockito_test1.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Testing Observable/Observer Interactions}

\begin{center}
\includegraphics[width=12cm]{pictures/mockito_test2.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Using Hamcrest matchers in Mockito JUnit Tests}

\begin{center}
\includegraphics[width=12cm]{pictures/mockito_matcher.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Stubbing the Behavior of Mocks}

\begin{center}
\includegraphics[width=12cm]{pictures/mockito_stubbing.png}
\end{center}

\begin{itemize}
\item \texttt{doThrow(e).when(mock).method(pars)}
\item \texttt{doReturn(r).when(mock).method(pars)}
\end{itemize}

\end{frame}

\end{document}
